package br.senai.sp.android_fic_escolas_dev.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import br.senai.sp.android_fic_escolas_dev.R;
import br.senai.sp.android_fic_escolas_dev.adapter.AlunoAdapter;
import br.senai.sp.android_fic_escolas_dev.dao.AlunoDao;
import br.senai.sp.android_fic_escolas_dev.model.Aluno;

public class MainActivity extends AppCompatActivity {

    private ListView lvListaAlunos;
    private AlunoDao dao = AlunoDao.manager;
    private List<Aluno> listagemDeAlunos;
    private AlunoAdapter alunoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // getSupportActionBar().setTitle("Lista de Alunos");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDeFormulario = new Intent(MainActivity.this, FormActivity.class);
                startActivityForResult(intentDeFormulario, 1);
            }
        });

        // fazendo referência das views
        lvListaAlunos = findViewById(R.id.lvListaAlunos);

        // Selecionando um aluno da lista
        lvListaAlunos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Aluno alunoSelecionado = (Aluno) adapterView.getAdapter().getItem(i);
                // Toast.makeText(getApplicationContext(), alunoSelecionado.toString(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, FormActivity.class);
                intent.putExtra("idAluno", alunoSelecionado.getId());
                startActivityForResult(intent, 1);
            }
        });

//        String[] listagemDeAlunos = new String[]{"Helena", "Thales", "Felipe"};
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listagemDeAlunos);
//        lvListaAlunos.setAdapter(adapter);

        carregarListaDeAlunos();

    }

    private void carregarListaDeAlunos() {
        listagemDeAlunos = dao.getLista();
        alunoAdapter = new AlunoAdapter(listagemDeAlunos, this);
        lvListaAlunos.setAdapter(alunoAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                carregarListaDeAlunos();
                Toast.makeText(getApplicationContext(), "Aluno inserido com sucesso.", Toast.LENGTH_LONG).show();
            }
        }
    }

}
