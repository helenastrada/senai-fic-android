package br.senai.sp.android_fic_escolas_dev.config.service;

import java.util.List;

import br.senai.sp.android_fic_escolas_dev.model.Aluno;
import br.senai.sp.android_fic_escolas_dev.model.Usuario;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RestInterface {

    @POST("auth/login")
    Call<ResponseBody> buscarLogin(@Body Usuario usuario);

    @GET("alunos")
    Call<List<Aluno>> listarAlunos();

    @POST("alunos")
    Call<Aluno> salvarAluno(@Body Aluno aluno);

    @GET("alunos/{id}")
    Call<Aluno> buscarAluno(@Path("id") Long id);

    @DELETE("alunos/{id}")
    void deletarAluno(@Path("id") Long id);


}
