package br.senai.sp.android_fic_escolas_dev.views;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.io.IOException;
import java.text.Normalizer;
import java.util.List;

import br.senai.sp.android_fic_escolas_dev.R;
import br.senai.sp.android_fic_escolas_dev.adapter.AlunoAdapter;
import br.senai.sp.android_fic_escolas_dev.bd.AlunoDaoDB;
import br.senai.sp.android_fic_escolas_dev.commons.AppUtils;
import br.senai.sp.android_fic_escolas_dev.config.RetrofitConfig;
import br.senai.sp.android_fic_escolas_dev.dao.AlunoDao;
import br.senai.sp.android_fic_escolas_dev.model.Aluno;
import br.senai.sp.android_fic_escolas_dev.rv.adapter.AlunoAdapterRV;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ListView lvListaAlunos;
    private RecyclerView rvListaAlunos;
    // private AlunoDao dao = AlunoDao.manager;
    private List<Aluno> listagemDeAlunos;
    private AlunoAdapter alunoAdapter;
    // private AlunoDaoDB dao = new AlunoDaoDB(this);

    private String token;
    private OkHttpClient okHttpClient;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDeFormulario = new Intent(MainActivity.this, FormActivity.class);
                startActivityForResult(intentDeFormulario, 1);
            }
        });

        sharedPreferences = getSharedPreferences(AppUtils.SHARED_KEY, Context.MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");

        rvListaAlunos = findViewById(R.id.rvListaAlunos);

        okHttpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request.Builder b = chain.request().newBuilder();
                b.addHeader("Accept", "application/json");
                b.addHeader("Authorization", token);
                return chain.proceed(b.build());
            }
        }).build();


        carregarListaDeAlunos(okHttpClient);

    }

    private void carregarListaDeAlunos(OkHttpClient okHttpClient) {
        // listagemDeAlunos = dao.getLista();

        Call<List<Aluno>> call = new RetrofitConfig(okHttpClient).getRestInterface().listarAlunos();
        call.enqueue(new Callback<List<Aluno>>() {
            @Override
            public void onResponse(Call<List<Aluno>> call, Response<List<Aluno>> response) {
                if (response.isSuccessful()) {
                    listagemDeAlunos = response.body();
                    if (listagemDeAlunos.size() > 0) {
                        rvListaAlunos.setAdapter(new AlunoAdapterRV(listagemDeAlunos, getApplicationContext()));
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        rvListaAlunos.setLayoutManager(layoutManager);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Aluno>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Erro ao carregar lista.", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                carregarListaDeAlunos(okHttpClient);
                Toast.makeText(getApplicationContext(), "Aluno inserido com sucesso.", Toast.LENGTH_LONG).show();
            }
        }
    }

}
