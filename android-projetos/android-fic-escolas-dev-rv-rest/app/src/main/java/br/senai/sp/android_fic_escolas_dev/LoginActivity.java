package br.senai.sp.android_fic_escolas_dev;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import br.senai.sp.android_fic_escolas_dev.commons.AppUtils;
import br.senai.sp.android_fic_escolas_dev.config.RetrofitConfig;
import br.senai.sp.android_fic_escolas_dev.dao.UsuarioDao;
import br.senai.sp.android_fic_escolas_dev.model.Usuario;
import br.senai.sp.android_fic_escolas_dev.views.MainActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity {

    // views
    private TextInputLayout tilLoginEmail;
    private TextInputLayout tilLoginSenha;
    private Button btLoginUsuario;

    // variáveis do usuário
    private String loginDigitado;
    private String senhaDigitada;

    private String token;
    SharedPreferences sharedPreferences;

    private UsuarioDao dao = UsuarioDao.manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // fazendo as referências das nossas views
        tilLoginEmail = findViewById(R.id.tilLoginEmail);
        tilLoginSenha = findViewById(R.id.tilLoginSenha);
        btLoginUsuario = findViewById(R.id.btLoginUsuario);

        tilLoginEmail.getEditText().setText("admin");
        tilLoginSenha.getEditText().setText("admin");

        btLoginUsuario.setOnClickListener(new verificarUsuarioDigitado());

        sharedPreferences = getSharedPreferences(AppUtils.SHARED_KEY, Context.MODE_PRIVATE);

    }

    private class verificarUsuarioDigitado implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            loginDigitado = tilLoginSenha.getEditText().getText().toString();
            senhaDigitada = tilLoginSenha.getEditText().getText().toString();
            Usuario usuarioConferirNaLista = new Usuario(loginDigitado, senhaDigitada);

            // RestInterface restInterface = retrofit.create(RestInterface.class);
            Call<ResponseBody> call = new RetrofitConfig().getRestInterface().buscarLogin(usuarioConferirNaLista);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    if(response.isSuccessful()) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("token", response.headers().get("authorization"));
                        editor.apply();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Erro: " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });


//            if (dao.localizar(usuarioConferirNaLista) != null) {
//                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                startActivity(intent);
//                LoginActivity.this.finish();
//
//            } else {
//                Toast.makeText(getApplicationContext(), "Usuário ou senha incorretos.", Toast.LENGTH_SHORT).show();
//            }

        }
    }
}
