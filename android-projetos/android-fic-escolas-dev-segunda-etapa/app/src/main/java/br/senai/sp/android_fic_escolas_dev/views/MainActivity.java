package br.senai.sp.android_fic_escolas_dev.views;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import br.senai.sp.android_fic_escolas_dev.R;
import br.senai.sp.android_fic_escolas_dev.adapter.AlunoAdapter;
import br.senai.sp.android_fic_escolas_dev.dao.AlunoDao;
import br.senai.sp.android_fic_escolas_dev.model.Aluno;

public class MainActivity extends AppCompatActivity {

    private ListView lvListaAlunos;
    private AlunoDao dao = AlunoDao.manager;
    private List<Aluno> listagemDeAlunos;
    private AlunoAdapter alunoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // getSupportActionBar().setTitle("Lista de Alunos");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // fazendo referência das views
        lvListaAlunos = findViewById(R.id.lvListaAlunos);

//        String[] listagemDeAlunos = new String[]{"Helena", "Thales", "Felipe"};
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, listagemDeAlunos);
//        lvListaAlunos.setAdapter(adapter);

        carregarListaDeAlunos();

    }

    private void carregarListaDeAlunos() {
        listagemDeAlunos = dao.getLista();
        alunoAdapter = new AlunoAdapter(listagemDeAlunos, this);
        lvListaAlunos.setAdapter(alunoAdapter);
    }

}
