package br.senai.sp.android_fic_escolas_dev.views;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;

import br.senai.sp.android_fic_escolas_dev.R;
import br.senai.sp.android_fic_escolas_dev.dao.AlunoDao;
import br.senai.sp.android_fic_escolas_dev.model.Aluno;

public class FormActivity extends AppCompatActivity {

    private TextInputLayout tilNomeAluno;
    private TextInputLayout tilDataNascimentoAluno;
    private TextInputLayout tilEnderecoAluno;
    private EditText etDataNascimentoAluno;
    private Button btnSalvarAluno;
    private ImageView ivFotoAluno;
    private AlunoDao dao = AlunoDao.manager;

    // dados do calendário para montar a data
    private int ano, mes, dia;

    // recursos para salvar a imagem
    private byte[] b;
    private String temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        getSupportActionBar().setTitle("Formulário");

        // Referências das views
        tilNomeAluno = findViewById(R.id.tilNomeAluno);
        tilDataNascimentoAluno = findViewById(R.id.tilDataNascimentoAluno);
        tilEnderecoAluno = findViewById(R.id.tilEnderecoAluno);
        etDataNascimentoAluno = findViewById(R.id.etDataNascimentoAluno);
        btnSalvarAluno = findViewById(R.id.btnSalvarAluno);
        ivFotoAluno = findViewById(R.id.ivFotoAluno);

        etDataNascimentoAluno.setOnClickListener(new abrirCalendario());
        ivFotoAluno.setOnClickListener(new abrirCamera());
        btnSalvarAluno.setOnClickListener(new salvarDadosDoAluno());

    }

    private class salvarDadosDoAluno implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            Aluno alunoNovo = new Aluno();
            alunoNovo.setNome(tilNomeAluno.getEditText().getText().toString());
            alunoNovo.setDataNascimento(new Date(tilDataNascimentoAluno.getEditText().getText().toString()));
            alunoNovo.setEndereco(tilEnderecoAluno.getEditText().getText().toString());

            alunoNovo.setFotoAluno(temp);
            dao.salvar(alunoNovo);
            Log.d("NovoAluno: ", dao.getLista().toString());
            retornarParaTelaAnteriorAposSalvar();
        }
    }

    private void retornarParaTelaAnteriorAposSalvar() {
        Intent retornarParaActivityHome = new Intent();
        retornarParaActivityHome.putExtra("result", 1);
        setResult(Activity.RESULT_OK, retornarParaActivityHome);
        finish();
    }


    // abrir um calendário
    private class abrirCalendario implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            // calendário
            final Calendar calendar = Calendar.getInstance();
            ano = calendar.get(Calendar.YEAR);
            mes = calendar.get(Calendar.MONTH);
            dia = calendar.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(FormActivity.this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            etDataNascimentoAluno.setText((monthOfYear + 1) + "/" + dayOfMonth + "/" + year);
                        }
                    }, ano, mes, dia);
            datePickerDialog.show();
        }
    }

    // abrir a camera
    private class abrirCamera implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            abrirIntentDeCamera();
        }

    }

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private void abrirIntentDeCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // starto uma nova activity para abrir a camera
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            // ao fechar a camera, eu preciso verificar se o status foi ok e além disso, pegar a imagem que foi tirada
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            // com a imagem, eu vou mostrar a imagem na nossa imageview
            ivFotoAluno.setImageBitmap(imageBitmap);
            // uma vez que eu tenho essa imagem, preciso salvá-la
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            b = baos.toByteArray();
            temp = Base64.encodeToString(b, Base64.DEFAULT);

        }
    }
}
